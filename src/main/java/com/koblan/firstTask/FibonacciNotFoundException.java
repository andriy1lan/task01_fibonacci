package com.koblan.firstTask;

public class FibonacciNotFoundException extends RuntimeException {

    public FibonacciNotFoundException(String message) {

        super(message);
    }

    public FibonacciNotFoundException(String message, Throwable cause) {

        super(message, cause);
    }

}


