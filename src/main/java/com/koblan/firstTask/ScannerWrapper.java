package com.koblan.firstTask;
import java.util.Scanner;
public class ScannerWrapper implements AutoCloseable  {
    Scanner sc;
    int i;
    public ScannerWrapper() {
        sc=new Scanner(System.in);
    }

    public boolean hasNext() {
        return sc.hasNext();
    }

    public int nextInt() {
        i++;
        return sc.nextInt();
    }

    public void close() throws Exception {
        sc.close();
        System.out.println("Closed MyResource");
        throw new Exception("Innerclose exception");
    }

    public static void main(String[] args) throws Exception {
        try(ScannerWrapper sw=new ScannerWrapper();) {
            int i=0;
            while (sw.hasNext() && i++<5)
                System.out.println(sw.nextInt());
        }
        catch (Exception e) {System.out.print("Not correct number enter a new: ");}
    }
}

