package com.koblan.firstTask;
import java.util.Scanner;

/**
 * Class for generating the interval of integer numbers from Console Input
 * and finding the odd, even numbers, its sum, and
 * finding the fibonacci numbers in it, and percentage of odd and even numbers among the last ones
 * @version 1.0 September 19, 2018
 * @author Andriy
 */

public class Fibo {

    int nums1[];

    int a;
    int b;

    int odds;
    int evens;

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void fillNums() {
        System.out.println();
        int length = b-a+1;
        nums1 = new int[length];
        for (int i = 0; i < length; i++) {
            nums1[i] = a + i;
        }
        System.out.println();
    }

    public void sumOfNums() {
        System.out.println("Prints the sum of odd and even numbers of sequence");
        int sum = 0;
        for (int i = 0; i < nums1.length; i++) {
            sum += nums1[i];
        }
        System.out.println(sum);
    }


    public void printOdd() {
        System.out.println("Prints the odd numbers of sequence");
        for (int i = 0; i < nums1.length; i++) {
            if (nums1[i] % 2 == 1) {
                System.out.print(nums1[i] + " ");
            }
        }
        System.out.println();
    }

    public void printEven() {
        System.out.println("Prints the even numbers of sequence");
        for (int i = nums1.length - 1; i>= 0; i--) {
            if (nums1[i] % 2 == 0) {
                System.out.print(nums1[i] + " ");
            }
        }
        System.out.println();
    }

    public void iterateFibo() {
        System.out.println("Program builds Fibonacci numbers and print F1 and F2 as the biggest odd and even numbers from initial interval");
        int f1 = 0;
        int f2 = 0;
        int fib = 1;
        int prevFib = 1;
        System.out.print("Fibo sequence in interval: ");
        while (fib <= nums1[nums1.length-1]) {
            int temp = fib;
            fib += prevFib;
            prevFib = temp;
            if ((fib >= nums1[0]) && (fib <= nums1[nums1.length - 1]))  {
                if (fib % 2 == 1) {
                    f1 = fib; odds++ ;}
                else if (fib % 2 == 0) {
                    f2 = fib; evens++ ; }
                System.out.print(fib + " ");
            }
        }
        System.out.println();
        System.out.println("F1:" + f1 + "");
        System.out.println("F2:" + f2 + "");
    }

    public void findPercentage() {
        int alls = odds + evens;
        ArithmeticException ae=new ArithmeticException();
        if (alls==0) throw new FibonacciNotFoundException("There is no fibo numbers", ae);
        float oddsf = odds * 100 /(float) alls;
        float evensf = evens * 100 /(float) alls;
        System.out.print("Percentage of odd fibo numbers:");
        System.out.printf("%.2f", oddsf);
        System.out.print("%");
        System.out.println();
        System.out.print("Percentage of even fibo numbers:");
        System.out.printf("%.2f", evensf);
        System.out.print("%");
    }

    public static void main(String[] args) {

        Fibo f = new Fibo();
        int c = 0;
        int d = 0;
        Scanner sc = new Scanner(System.in);
        try {
            do {
                System.out.println("Please enter a positive number as start of interval");
                while (!sc.hasNextInt()) {
                    System.out.println("That is not a positive number");
                    sc.next(); }
                c = sc.nextInt();
            } while (c < 0);
            do {
                System.out.println("Please enter a positive number as end of interval, but grosser then entered before");
                while (!sc.hasNextInt()) {
                    System.out.println("That's not a correct number");
                    sc.next(); }
                d = sc.nextInt();
            } while (d <= c);
        } catch (Exception ex) {
            System.out.print(ex.toString()); }
        f.setA(c);
        f.setB(d);
        f.fillNums();

        System.out.print("Do you need to see the interval's odd and even numbers - Type 'y' or 'Y' \n - otherswise type any other character");
        Scanner sc1 = new Scanner(System.in);
        if (sc1.hasNext("y") || sc1.hasNext("Y")) {
            f.printOdd();
            f.printEven();
        }

        System.out.print("To see odd and even numbers sum- Type 'y'/'Y'");
        Scanner sc2 = new Scanner(System.in);
        if(sc2.hasNext("y") || sc2.hasNext("Y")) {
            f.sumOfNums();
        }

        System.out.print("To see Fibonacci numbers list and percentage of \n odd and even numbers - Type 'f'/'F'");
        Scanner sc3 = new Scanner(System.in);
        if (sc3.hasNext("f") || sc3.hasNext("F")) {
            f.iterateFibo();
            try {
                f.findPercentage();
            }
            catch (FibonacciNotFoundException e) {
                System.out.println(e.getMessage()+" "+e.getCause()); }
        }
    }
}
